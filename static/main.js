const debounce = (func, wait) => {
  let timeout

  return function (...args) {
    const context = this

    clearTimeout(timeout)

    timeout = setTimeout(() => func.apply(context, args), wait)
  }
}

const fetchResults = () => {
  const formData = new FormData()
  formData.append(
    'mathdict_query', document.getElementById('mathdict_query').value
  )

  return fetch(window.location.href, {
    method: 'POST',
    headers: {
      'Accept': 'text/html'
    },
    body: formData
  })
  .then(response => response.text())
  .then(text => {
    const parser = new DOMParser()
    const htmlDocument = parser.parseFromString(text, 'text/html')

    return htmlDocument.getElementById('search_results').innerHTML
  })
  .catch(err => {
    console.error(`Error while fetching results:\n${err}`)

    return '<div id="no_results">Error while fetching results! · ' +
      'Hiba a találatok betöltése közben!</div>'
  })
}

const updateResults = (markup) => {
  document.getElementById('search_results').innerHTML = markup
}

window.addEventListener('DOMContentLoaded', () => {
  document.getElementById('search_form').addEventListener('submit', event => {
    event.preventDefault()
  })

  const searchInput = document.getElementById('mathdict_query')

  const updateWhileTyping = debounce(async () => {
    if (searchInput.value.trim() === '') {
      updateResults('')
    }

    updateResults(await fetchResults())
  }, 200)

  searchInput.addEventListener(
    'keyup', updateWhileTyping
  )
})

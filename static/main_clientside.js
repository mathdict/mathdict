/*
mathdict
Copyright (C) 2019-2022  mathdict  mathdict@mathdict.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const debounce = (func, wait) => {
  let timeout

  return function (...args) {
    const context = this

    clearTimeout(timeout)

    timeout = setTimeout(() => func.apply(context, args), wait)
  }
}

const translate = (input, replacements) => {
  return [...input].map(ch => replacements[ch] || ch).join('');
}

const transl_table = {
  'á':'a',
  'é':'e',
  'ő':'o',
  'ö':'o',
  'ü':'u',
  'ó':'o',
  'ú':'u',
  'ű':'u',
  'í':'i'
}

const canonicalForm = (word) => {
    return translate(word.toLowerCase(), transl_table).replace(/[^0-9a-z]/gi, '')
}

const fetchResults = () => {
  const query = canonicalForm(document.getElementById('mathdict_query').value);

  document.getElementById('no_results_too_short').style.display = 'none';
  document.getElementById('no_results').style.display = 'none';
  const table = document.getElementById('result_table');
  table.innerHTML = '';

  if(query.length < 3) {
    document.getElementById('no_results_too_short').style.display = 'block';
    return;
  }

  let results = [];
  for(let i = 0; i < dictionary.length; ++i) {
    if(dictionary[i]['hu_search'].includes(query) || dictionary[i]['en_search'].includes(query)) {
      results.push(dictionary[i]);
    }
  }

  if(results.length === 0) {
    document.getElementById('no_results').style.display = 'block';
    return;
  }

  const header = '<tr><th>Magyar</th><th>English</th></tr><tr>';
  let resultsHtml = '';
  for(result of results) {
    resultsHtml += `<tr><td>${result.hu}${result.hu_note.length > 0 ? ' <div class="note">'+result.hu_note+'</div>' : ''}</td>`
    resultsHtml += `<td>${result.en}${result.en_note.length > 0 ? ' <div class="note">'+result.en_note+'</div>' : ''}</td></tr>`
  }
  table.innerHTML = header + resultsHtml;
}

window.addEventListener('DOMContentLoaded', () => {
  document.getElementById('search_form').addEventListener('submit', event => {
    event.preventDefault()
  })
  document.getElementById('wordcount_text').innerHTML = `${dictionary.length} szópár &#183; ${dictionary.length} word pairs`

  const searchInput = document.getElementById('mathdict_query')

  const updateWhileTyping = debounce(async () => {
    await fetchResults()
  }, 200)

  searchInput.addEventListener(
    'keyup', updateWhileTyping
  )
})

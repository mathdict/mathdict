import sqlite3
import os
import csv
import json

transl_table = str.maketrans('áéőöüóúűí','aeoououui')

def canonical_form(word):
    word = word.lower().translate(transl_table)
    res = ""
    for chr in word:
        if chr.isalpha():
            res += chr
    return res

datapath = os.path.dirname(os.path.abspath(__file__))+'/data/'

try:
    os.remove(datapath+"/words.db")
except:
    pass

db = sqlite3.connect(datapath+"/words.db")
db.row_factory = sqlite3.Row
c = db.cursor()
c.execute('create table dict(en_search text, hu_search text, en text, hu text, en_note text, hu_note text)')

json_data = []

with open(datapath+'/words.tsv', encoding='utf-8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter='\t')
    for row in csv_reader:
        hun_note_done = False
        notes_text_hu = ''
        notes_text_en = ''
        for note in row[2:]:
            if not len(note):
                hun_note_done = True
                continue
            if note.startswith('http') and 'wikipedia' in note:
                note = '<a href="'+note+'">wiki</a>'
            if hun_note_done:
                notes_text_en += ', ' + note
            else:
                notes_text_hu += ', ' + note
            if notes_text_en.startswith(', '): notes_text_en = notes_text_en[2:]
            if notes_text_hu.startswith(', '): notes_text_hu = notes_text_hu[2:]
        c.execute('insert into dict values(?, ?, ?, ?, ?, ?)', (canonical_form(row[1]), canonical_form(row[0]), row[1], row[0], notes_text_en.strip(), notes_text_hu.strip()))
        json_data.append({
            'en_search': canonical_form(row[1]),
            'hu_search': canonical_form(row[0]),
            'en': row[1],
            'hu': row[0],
            'en_note': notes_text_en.strip(),
            'hu_note': notes_text_hu.strip()
        })

    c.execute('create index dictindx on dict(en_search, hu_search)')

    db.commit()
    db.close()

with open(datapath+'/../static/data_clientside.js', 'w', encoding='utf-8') as f:
    f.write(f'const dictionary={json.dumps(json_data)}')

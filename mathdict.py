#!/usr/bin/python3

#mathdict
#Copyright (C) 2019  mathdict  mathdict@mathdict.org
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import datetime
import math
import time
import random
import shutil
import hashlib
import json
from subprocess import Popen
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, jsonify, send_from_directory
import sqlite3
import shlex

app = Flask(__name__)
app.config.from_object(__name__)

transl_table = str.maketrans('áéőöüóúűí','aeoououui')

def canonical_form(word):
    word = word.lower().translate(transl_table)
    res = ""
    for chr in word:
        if chr.isalpha():
            res += chr
    return res

@app.route('/', methods=['GET', 'POST'])
def query_page():
    render_start = datetime.datetime.now()
    db = sqlite3.connect(os.path.dirname(os.path.abspath(__file__))+'/data/words.db')
    db.row_factory = sqlite3.Row
    c = db.cursor()
    c.execute('select count(*) c from dict')
    word_pair_count = c.fetchone()['c']
    query = ''
    search_res = []
    fail_too_short = False
    fail_no_result = False
    if request.method == 'POST':
        if 'mathdict_query' in request.form:
            query = request.form['mathdict_query']
            if len(query):
                if len(query) < 3:
                    fail_too_short = True
                else:
                    query = canonical_form(query)
                    c.execute('select * from dict where en_search like ? or hu_search like ?', ('%'+query+'%', '%'+query+'%'))
                    for row in c.fetchall():
                        search_res.append([row['en'],row['hu'],row['en_note'],row['hu_note']])
                    if not len(search_res): fail_no_result = True
    render_time = int((datetime.datetime.now() - render_start)/datetime.timedelta(microseconds=1))
    return render_template('main.html', results=search_res, query=query, word_pair_count=word_pair_count, fail_no_result=fail_no_result, fail_too_short=fail_too_short, render_time=render_time)

@app.route('/favicon.png')
def send_favicon():
    return send_from_directory(os.path.dirname(os.path.abspath(__file__))+'/static/', 'favicon.png')

if __name__ == "__main__":
    app.run()
